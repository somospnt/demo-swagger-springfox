package com.somospnt.demoswagger.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import static springfox.documentation.builders.PathSelectors.any;
import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfiguration.Constants;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final String VALIDATOR_URL = "validatorUrl";

    @Value("${swagger.base-package}")
    private String basePackage;

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(basePackage(basePackage))
                .paths(any())
                .build();
    }

    @Bean
    public UiConfiguration uiConfiguration() {
        return new UiConfiguration(VALIDATOR_URL, Constants.NO_SUBMIT_METHODS);
    }

}
