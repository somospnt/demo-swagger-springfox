package com.somospnt.demoswagger.controller;

import com.somospnt.demoswagger.domain.Color;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/color/")
public class ColorRestController {

    @GetMapping("rojo")
    public Color obtenerRojo() {
        Color color = new Color();
        color.setCodigo("#FF0000");
        color.setNombre("rojo");

        return color;
    }

    @GetMapping("azul")
    public Color obtenerAzul() {
        Color color = new Color();
        color.setCodigo("#0000FF");
        color.setNombre("azul");

        return color;
    }

    @PostMapping
    public Color cargar(@RequestBody Color color) {
        return color;
    }

}
