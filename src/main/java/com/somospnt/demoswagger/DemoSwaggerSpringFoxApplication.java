package com.somospnt.demoswagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSwaggerSpringFoxApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSwaggerSpringFoxApplication.class, args);
    }
}
