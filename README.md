# DEMO SWAGGER - SPRINGFOX #



## Primero que nada ¿qué es Swagger?



Swagger nos permite describir y documentar una API REST, para que otros puedan saber como funciona sin tener que estar viendo el código fuente o leyendo la documentación correspondiente.
**Es una especificación formal** que define un set de archivos necesarios para definir una api, los cuales son utilizados por diversas herramientas, como por ejemplo, interfaces de usuario.



## ¿Cómo implementamos Swagger en este proyecto?



En este ejemplo, vamos a usar una librería que se llama **Springfox**, la cual implementa la **especificación Swagger 2.0**. La ventaja de usar esta librería es que, al integrarse con Spring MVC, requiere muy poca configuración para integrarla en nuestro proyecto (si no disponemos de Spring Boot, igualmente sigue siendo simple).



## Configuración



1) Agregamos al POM las siguientes dependencias:


```
#!java

        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>2.5.0</version>
        </dependency>
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>2.5.0</version>
        </dependency>
```

2) Creamos la siguiente clase de configuración:


```
#!java

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final String BASE_PACKAGE = "com.somospnt.demoswagger";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(basePackage(BASE_PACKAGE))
                .paths(any())
                .build();
    }

}
```

3) Este paso se hace si no disponemos de Spring Boot en el proyecto:

Crear una clase que extienda de WebMvcConfigurerAdapter, agregarle la anotación @EnableWebMvc e implementar el siguiente método:


```
#!java
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("swagger-ui.html")
      .addResourceLocations("classpath:/META-INF/resources/");
 
    registry.addResourceHandler("/webjars/**")
      .addResourceLocations("classpath:/META-INF/resources/webjars/");
}

```

Y....LISTO! Ya tenemos Swagger configurado en nuestro proyecto :)

Para acceder a la interfaz gráfica, accedemos a la siguiente url:

http://<ip>/<app-root>/swagger-ui.html

Links de interés:

[Página oficial de Swagger](http://swagger.io/)

[Guía paso a paso](http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api)